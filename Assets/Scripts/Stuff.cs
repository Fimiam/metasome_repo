using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Stuff : TrackedObject
{
    public int level;

    public override bool CanBeEaten(Drop drop)
    {
        return drop.score >= level;
    }

    public override bool CanBeTracked(Drop drop)
    {
        return !sucked;
    }

    public override void Eaten(Drop drop)
    {
        var seq = DOTween.Sequence();

        seq.Append(transform.DOMove(drop.transform.position, .1f));
        seq.Join(transform.DOScale(0, .3f));

        seq.onComplete += () => { drop.Ate(this); };
    }
}
