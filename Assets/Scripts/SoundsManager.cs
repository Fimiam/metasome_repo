using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundsManager : Singleton<SoundsManager>
{
    [SerializeField] private List<AudioSource> sfxes;

    private SoundsData soundsData;

    private void Start()
    {
        soundsData = Game.Instance.Persistance.GetData<SoundsData>();
    }

    public void PlayAddSFX(int index)
    {
        if (!soundsData.SoundActive) return;

        if (index > sfxes.Count - 1) index = sfxes.Count - 1;

        sfxes[index].Play();
    }
}
