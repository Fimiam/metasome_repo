﻿using System;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using Facebook.Unity;
using GameAnalyticsSDK;

public class Game : Singleton<Game>
{
    public GameConfigurations gameConfigurations;

    public static bool IsTabletScreen => (float)Screen.height / (float)Screen.width < 1.5f;

    public PersistanceManager Persistance { get; private set; }

    public ProgressData ProgressData { get; private set; }
    
    public VibrationsManager Vibrations { get; private set; }

    protected override void Awake()
    {
        base.Awake();

        Persistance = new PersistanceManager();

        ProgressData = Persistance.GetData<ProgressData>();
        
        Vibrations = new VibrationsManager();

        Vibrations.ActivateVibration();

        Application.targetFrameRate = 60;
    }

    private void Start()
    {
        //analytics initialization

        GameAnalytics.Initialize();

        FB.Init();
    }


    public void LevelComplete()
    {
        //int nextLevel = ProgressData.CurrentLevel + 1;

        //nextLevel = nextLevel >= 0/*configs.LevelsConfig.LevelsCount*/ ? 0 : nextLevel;

        //ProgressData.SetCurrentLevel(nextLevel);

        //ProgressData.IncreaseProgressionLevel();
    }

#if UNITY_EDITOR

    [MenuItem("Cheating/Add Soft")]
    public static void AddSoft()
    {
        //Instance.ProgressData.AddSoft(5000);
    }


#endif
}

[Serializable]
public class Numbers
{
    public int Number;
    public string ViewString;
    public Material Material;
}
