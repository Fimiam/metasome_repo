using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TrackedObject : MonoBehaviour
{
    public Action<TrackedObject> OnDisactive;

    public bool sucked;

    public abstract bool CanBeEaten(Drop drop);
    public abstract bool CanBeTracked(Drop drop);
    public abstract void Eaten(Drop drop);

    public void Disactivate()
    {
        OnDisactive?.Invoke(this);
    }

}
