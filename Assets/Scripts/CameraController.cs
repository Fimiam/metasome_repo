using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private Transform target;
    [SerializeField] private Vector3 positionOffset, lookAtOffset;
    [SerializeField] private float positionSmooth = .2f;

    private void LateUpdate()
    {
        var pos = target.position + positionOffset;

        transform.position = Vector3.Lerp(transform.position, pos, positionSmooth);

        var lookDir = (target.position + lookAtOffset) - transform.position;

        transform.rotation = Quaternion.LookRotation(lookDir.normalized);
    }
}
