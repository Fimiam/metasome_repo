using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class MainDrop : Drop
{
    [SerializeField] private Transform eyesParent;
    [SerializeField] private float eyesLookSmooth = .4f;
    [SerializeField] private AnimationCurve maxHeightCurve, heightDistanceCurve, maxHeightDistanceMult, eyesHeightCurve;

    [HideInInspector] public List<Drop> drops;

    private Bounds dropsBounds;
    [SerializeField] private BoxCollider boxCollider;

    [SerializeField] private Drop drop_prefab;

    [SerializeField] private bool imBot;
    private Vector2 tapPoint;

    [SerializeField] private float speed = 2;

    [SerializeField] private float triggerExpand = 1;

    private float toNextBotDirection;

    public List<TrackedObject> tracking = new List<TrackedObject>();

    public bool mainDrop;

    private TrackedObject lastSpawnedDrop;

    [Space(5)]
    [Header("vac")]
    [SerializeField] private Vac vacPrefab;

    protected override void Start()
    {
        base.Start();

        drops.Add(this);

        UpdateBounds();

        mainDrop = true;

        StartCoroutine(UpdatingTracked());
    }

    void Update()
    {
        if (!imBot)
        {
            if (Input.GetMouseButtonDown(0))
            {
                tapPoint = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            }

            if(Input.GetMouseButton(0))
            {
                Vector2 mousePos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

                if ((mousePos - tapPoint).magnitude != 0.0f)
                {
                    var camDir = Camera.main.transform.forward;

                    camDir.y = 0;

                    var moveDir = new Vector3((mousePos - tapPoint).normalized.x, 0, (mousePos - tapPoint).normalized.y);

                    moveDir = Quaternion.LookRotation(camDir.normalized) * moveDir;

                    targetVelocity = speed * moveDir;

                    rigidbody.velocity = Vector2.Lerp(rigidbody.velocity, targetVelocity, velocitySmooth);
                }
            }

        }
        else
        {
            toNextBotDirection -= Time.deltaTime;

            if(toNextBotDirection < 0.0f)
            {
                toNextBotDirection = Random.Range(1, 2);

                var randomDir = Random.insideUnitSphere;

                randomDir.y = 0;

                SetDropVelocity(randomDir * speed);

            }


        }

        HeightUpdate();
        UpdateDrops();
        UpdateDropsVelocity();
        UpdateBounds();

        var targetRot = Quaternion.LookRotation(rigidbody.velocity.normalized);

        eyesParent.rotation = Quaternion.Lerp(eyesParent.rotation, targetRot, eyesLookSmooth);
    }

    private IEnumerator UpdatingTracked()
    {
        float dist = default, vacDist = default;


        while(true)
        {
            vacDist = Game.Instance.gameConfigurations.PlayerConfigs.vacDistance;

            if (vacDist > 0)
            {

                var tempTrack = new TrackedObject[tracking.Count];
                var tempDrops = new Drop[drops.Count];

                tracking.CopyTo(tempTrack);
                drops.CopyTo(tempDrops);

                foreach (var item in tempTrack)
                {

                    foreach (var drop in tempDrops)
                    {
                        dist = (item.transform.position - drop.transform.position).sqrMagnitude;

                        if(dist < vacDist * vacDist)
                        {
                            var vac = Instantiate(vacPrefab);

                            vac.VacStuff(drop, item);

                            tracking.Remove(item);

                            break;
                        }
                    }

                    //yield return null;
                }

            }


            yield return null;
        }

    }

    private void HeightUpdate()
    {
        var minHeight = maxHeightCurve.Evaluate(0);

        var currentMax = maxHeightCurve.Evaluate((float)score);

        var centerDist = (transform.position - dropsBounds.center).sqrMagnitude;

        currentMax = Mathf.Lerp(minHeight, currentMax, maxHeightDistanceMult.Evaluate(centerDist));

        float dist = default;

        foreach (var item in drops)
        {
            dist = (item.transform.position - transform.position).sqrMagnitude;

            item.SetHeight(Mathf.Lerp(minHeight, currentMax, heightDistanceCurve.Evaluate(dist)));
        }

        var eyesHeight = eyesHeightCurve.Evaluate(Mathf.InverseLerp(minHeight, 1, height));

        eyesParent.localPosition = Vector3.up * eyesHeight;
    }

    private void UpdateDrops()
    {
        foreach (var item in drops)
        {
            item.UpdateDrop();
        }
    }

    private void UpdateBounds()
    {
        dropsBounds = new Bounds(transform.position, Vector3.one);

        foreach (var item in drops)
        {
            dropsBounds.Encapsulate(item.transform.position);
        }

        dropsBounds.Expand(triggerExpand);

        boxCollider.center = transform.InverseTransformPoint(dropsBounds.center);
        boxCollider.size = transform.InverseTransformVector(dropsBounds.size);
    }

    private void UpdateDropsVelocity()
    {
        Vector3 mainDir = default;

        for (int i = 1; i < drops.Count; i++)
        {
            mainDir = drops[0].transform.position - drops[i].transform.position;

            drops[i].SetDropVelocity(rigidbody.velocity.magnitude * mainDir.normalized);
        }
    }

    public void AddDrop(Drop drop)
    {
        if(drop.owner)
            drop.owner.LoseDrop(drop);

        drop.owner = this;
        drop.SetColor(color, true);
        drop.SetSkin(LiquidSkinsControl.Instance.CurrentSkin);
        score++;

        foreach (var item in drops)
        {
            item.score = score;
        }

        drops.Add(drop);

    }

    public override void SetSkin(SkinItem skinItem)
    {
        colorSpriteRenderer.color = color = skinItem.color;

        if(skinItem.skinTexture)
        {
            var skinColor = new Color(1, skinItem.skinGValue, 0);

            skinSpriteRenderer.color = skinColor;
        }
        else
        {
            skinSpriteRenderer.color = Color.black;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.TryGetComponent(out TrackedObject obj))
        {
            if(obj.CanBeTracked(this) && obj != lastSpawnedDrop)
                AddTrackable(obj);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent(out TrackedObject obj))
        {
            RemoveTrackable(obj);
        }
    }

    private void AddTrackable(TrackedObject obj)
    {
        if (tracking.Contains(obj)) return;

        obj.OnDisactive += TrackableDisactive;

        tracking.Add(obj);
    }

    private void RemoveTrackable(TrackedObject obj)
    {
        tracking.Remove(obj);
    }

    private void TrackableDisactive(TrackedObject obj)
    {
        tracking.Remove(obj);
    }

    public override void LoseDrop(Drop drop)
    {
        drops.Remove(drop);
    }

    public override void Ate(TrackedObject stuff)
    {
        Destroy(stuff.gameObject);

        var newDrop = Instantiate(drop_prefab);

        lastSpawnedDrop = newDrop;

        AddDrop(newDrop);

        newDrop.transform.position = transform.position;
        newDrop.transform.localScale = Vector3.zero;
        newDrop.transform.DOScale(1, .3f);

    }
}
