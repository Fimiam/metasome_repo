using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropEaterTrigger : MonoBehaviour
{
    private Drop drop;

    private void Awake()
    {
        drop = transform.GetComponentInParent<Drop>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out TrackedObject obj))
        {
            Debug.Log("3");

            if(obj.CanBeEaten(drop))
            {
                Debug.Log("asd");

                drop.Eat(obj);
            }
        }


    }
}
