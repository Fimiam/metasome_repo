using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Liquid : MonoBehaviour
{
    public Transform target;

    [SerializeField] private Vector3 offset;


    void LateUpdate()
    {
        transform.position = target.position + offset;
    }
}
