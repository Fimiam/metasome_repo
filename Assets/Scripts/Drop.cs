using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

[RequireComponent(typeof(Rigidbody))]
public class Drop : TrackedObject
{
    [HideInInspector] public int score = 1;

    public Drop owner;

    public Color color;

    [SerializeField] protected SpriteRenderer heightSpriteRenderer, colorSpriteRenderer, skinSpriteRenderer;

    private float targetHeight;

    public float height = .45f;


    [SerializeField] protected float heightSmooth = .2f, velocitySmooth = .2f, settingColorDuration = .3f;

    public new Rigidbody rigidbody;


    protected Vector3 targetVelocity, velocity;

    protected virtual void Start()
    {
        owner = this;

        targetHeight = height;

        heightSpriteRenderer.color = new Color(height, height, height, 1);

        colorSpriteRenderer.color = color;
    }

    public void UpdateDrop()
    {
        height = Mathf.Lerp(height, targetHeight, heightSmooth);

        heightSpriteRenderer.color = new Color(height, height, height, 1);

        velocity = Vector3.Lerp(velocity, targetVelocity, velocitySmooth);

        rigidbody.velocity = velocity;
    }

    public void Eat(TrackedObject obj)
    {
        obj.Disactivate();

        obj.Eaten(this);
    }

    public virtual void Ate(TrackedObject stuff)
    {
        Destroy(stuff.gameObject);
    }

    public void SetDropVelocity(Vector3 velocity)
    {
        targetVelocity = velocity;
    }

    public void SetHeight(float height)
    {
        targetHeight = height;
    }

    public void SetColor(Color color, bool quick = false)
    {
        StartCoroutine(SettingColor(color, quick));
    }

    private IEnumerator EatingObject(TrackedObject obj)
    {
        yield return null;
    }

    private IEnumerator SettingColor(Color color, bool quick = false)
    {
        if(quick)
        {
            colorSpriteRenderer.color = this.color = color;

            yield break;
        }

        float t = 0.0f;

        Color startColor = this.color;

        yield return new WaitForSeconds(.2f);

        while(t < settingColorDuration)
        {
            t += Time.deltaTime;

            this.color = Color.Lerp(startColor, color, t / settingColorDuration);

            colorSpriteRenderer.color = this.color;

            yield return null;
        }
    }

    public virtual void SetSkin(SkinItem skinItem)
    {
        colorSpriteRenderer.color = color = skinItem.color;

        if (skinItem.skinTexture)
        {
            var skinColor = new Color(1, skinItem.skinGValue, 0);

            skinSpriteRenderer.color = skinColor;
        }
        else
        {
            skinSpriteRenderer.color = Color.black;
        }
    }

    public virtual void LoseDrop(Drop drop)
    {
        
    }

    public override bool CanBeEaten(Drop drop)
    {
        return drop.owner != null && drop.owner != owner && drop.score > score;
    }

    public override bool CanBeTracked(Drop drop)
    {
        return drop.owner != owner;
    }

    public override void Eaten(Drop drop)
    {
        
    }
}
