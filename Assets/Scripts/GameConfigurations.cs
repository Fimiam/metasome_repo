﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "config", menuName = "GameConfigs/mainConfig")]
public class GameConfigurations : ScriptableObject
{
    public SkinConfigs skinConfigs;
    public PlayerConfigs PlayerConfigs;
    public LevelsConfig LevelsConfig;
    public CommonConfigs CommonConfigs;
    public InputConfigs InputConfigs;
    public ChallangeConfigs Challanges;
}


[System.Serializable]
public struct Opponents
{
    [Range(0, 100)]
    public float BlockChance;

    public float StrikesFrequency;
}

[System.Serializable]
public struct CommonConfigs
{
    public float finalPunchPower;
}

[System.Serializable]
public struct LevelsConfig
{
    public List<LevelConfig> Levels;

    public int LevelsCount => Levels.Count;
}


[System.Serializable]
public struct PlayerConfigs
{
    public float vacDistance;
}

[System.Serializable]
public struct SkinConfigs
{
    public List<int> initUnlockedSkins;

    public List<SkinClass> skins;

    [Serializable]
    public class SkinClass
    {

    }

}

[System.Serializable]
public struct ChallangeConfigs
{
    public List<ChallangeConfig> challanges;

    public ChallangeConfig GetNextChallange(ChallangeType type, int currentValue)
    {
        ChallangeConfig challange = null;

        var availableChallanges = challanges.FindAll(c => c.Type == type);

        for (int i = 0; i < availableChallanges.Count; i++)
        {
            if (availableChallanges[i].CompleteValue > currentValue)
            {
                challange = availableChallanges[i];
                
                break;;
            }
        }

        return challange;
    }
}


[System.Serializable]
public struct InputConfigs
{
    
}

[System.Serializable]
public struct VibrationConfigs
{
    public VibrationConfigs.Android AndroidVibration;

    [System.Serializable]
    public struct Android
    {
        public int Shoot;
        public int Crystal;
        public int Death;
        public int Buttons;

        public long[] KillerPattern;
        public long[] WinPattern;
    }
}
