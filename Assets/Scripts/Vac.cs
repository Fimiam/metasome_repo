using Dreamteck.Splines;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vac : MonoBehaviour
{
    [SerializeField] private Transform spritesTransform;

    [SerializeField] private SpriteRenderer colorSpriteRenderer, heightSpriteRenderer, skinSpriteRenderer;

    [SerializeField] private SplineComputer spline;

    [SerializeField] private Renderer colorRenderer, heightRenderer, skinRenderer;

    [SerializeField] private float suckSpeed = 1;

    [SerializeField] private AnimationCurve speedCurve;

    private Drop drop;
    private TrackedObject suckedObj;

    public void VacStuff(Drop drop, TrackedObject item)
    {
        transform.position = drop.transform.position;

        colorSpriteRenderer.color = colorRenderer.material.color = drop.color;

        this.drop = drop;
        suckedObj = item;

        suckedObj.sucked = true;

        StartCoroutine(Sucking());
    }

    private void Update()
    {
        spline.SetPointPosition(0, drop.transform.position);
        spline.SetPointPosition(1, spritesTransform.position);
    }

    private IEnumerator Sucking()
    {
        float dist = float.MaxValue;

        float speed = speedCurve.Evaluate(dist);

        while(dist > 0.01f)
        {
            spritesTransform.position = Vector3.MoveTowards(spritesTransform.position, suckedObj.transform.position, speed * Time.deltaTime);

            dist = (spritesTransform.position - suckedObj.transform.position).sqrMagnitude;

            speed = speedCurve.Evaluate(dist);

            yield return null;
        }

        spritesTransform.position = suckedObj.transform.position;

        dist = float.MaxValue;

        while(dist > 0.01f)
        {
            spritesTransform.position = Vector3.MoveTowards(spritesTransform.position, drop.transform.position, speed * Time.deltaTime);

            suckedObj.transform.position = spritesTransform.position;

            dist = (spritesTransform.position - drop.transform.position).sqrMagnitude;

            speed = speedCurve.Evaluate(dist);

            yield return null;
        }

        suckedObj.sucked = false;

        gameObject.name = suckedObj.name;

        gameObject.SetActive(false);

        //Destroy(gameObject);
    }
}
