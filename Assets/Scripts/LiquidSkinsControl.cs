using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiquidSkinsControl : MonoBehaviour
{
    public static LiquidSkinsControl Instance;

    readonly int skinTexProperty = Shader.PropertyToID("_SkinTexArray");

    [SerializeField] private MeshRenderer liquidRenderer;

    [SerializeField] private List<SkinItem> skins;

    [SerializeField] private Drop playerMainDrop;

    public SkinItem CurrentSkin => skins[currentSkinIndex];

    private int currentSkinIndex;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        SetSkin(CurrentSkin);
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.RightArrow))
        {
            currentSkinIndex++;

            if (currentSkinIndex > skins.Count - 1) currentSkinIndex = 0;

            SetSkin(CurrentSkin);
        }

        if(Input.GetKeyDown(KeyCode.LeftArrow))
        {
            currentSkinIndex--;

            if (currentSkinIndex < 0) currentSkinIndex = skins.Count - 1;

            SetSkin(CurrentSkin);
        }
    }

    private void SetSkin(SkinItem skin)
    {
        if(skin.skinTexture)
            liquidRenderer.material.SetTexture(skinTexProperty, skin.skinTexture);

        playerMainDrop.SetSkin(skin);
    }
}

[System.Serializable]
public class SkinItem
{
    [ColorUsage(false)] public Color color;

    public Texture2DArray skinTexture;

    public float skinGValue;
}
