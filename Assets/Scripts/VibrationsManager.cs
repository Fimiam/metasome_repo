﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.NiceVibrations;

public class VibrationsManager
{
    private SoundsData soundsState;

    private float hapticDelay = 0.15f, lastHaptic;

    public void ActivateVibration()
    {
        MMVibrationManager.SetHapticsActive(true);

        soundsState = Game.Instance.Persistance.GetData<SoundsData>();
    }

    public void MergedVibro()
    {
        if (!soundsState.VibrationFull/* || Time.time - hapticDelay < Time.time*/) return;

        MMVibrationManager.Haptic(HapticTypes.HeavyImpact, false, true);

        Debug.Log("merged vibro");

        lastHaptic = Time.time;
    }

    public void HexAddVibro()
    {
        if (!soundsState.VibrationFull/* || Time.time - hapticDelay < Time.time*/) return;

        MMVibrationManager.Haptic(HapticTypes.MediumImpact, false, true);

        Debug.Log("hex add vibro");

        lastHaptic = Time.time;
    }
    
    public void ButtonClickVibro()
    {
        if (!soundsState.VibrationFull) return;

        MMVibrationManager.Haptic(HapticTypes.SoftImpact); 
    }
}
