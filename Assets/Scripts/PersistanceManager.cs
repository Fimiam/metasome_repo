﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersistanceManager
{
    private List<SavingData> datas;

    public PersistanceManager()
    {
        datas = new List<SavingData>();

        datas.Add(InitData<ProgressData>("progress"));
        datas.Add(InitData<StateData>("state"));
        datas.Add(InitData<SoundsData>("sounds"));
    }

    public T GetData<T>() where T : SavingData
    {
        return datas.Find(d => d.GetType() == typeof(T)) as T;
    }

    public SavingData InitData<T>(string _savingKey) where T : SavingData, new()
    {
        T data;

        if (ZPlayerPrefs.HasKey(_savingKey))
        {
            string toLoad = ZPlayerPrefs.GetString(_savingKey);

            data = JsonUtility.FromJson<T>(toLoad);
        }
        else
        {
            data = new T();
        }

        data.SetKey(_savingKey);

        return data;
    }
}

[Serializable]
public class SavingData
{
    protected string savingKey;

    public void SetKey(string _key) => savingKey = _key;

    protected void Save()
    {
        string toSave = JsonUtility.ToJson(this);

        ZPlayerPrefs.SetString(savingKey, toSave);
        ZPlayerPrefs.Save();
    }
}

[Serializable]
public class SoundsData : SavingData
{
    public event Action<bool> OnSoundsChanged;
    public event Action<bool> OnVibrationChanged;

    public bool SoundActive;
    public bool VibrationFull;

    public SoundsData()
    {
        SoundActive = true;
        VibrationFull = true;
    }

    public void SetSound(bool _active)
    {
        SoundActive = _active;
        OnSoundsChanged?.Invoke(_active);
        Save();
    }

    public void SetVibration(bool _full)
    {
        VibrationFull = _full;
        OnVibrationChanged?.Invoke(_full);
        Save();
    }
}

[Serializable]
public class StateData : SavingData
{
    public bool IsQuickStart;

    public StateData()
    {
        IsQuickStart = false;
    }

    public void SetQuickStart(bool value)
    {
        IsQuickStart = value;

        Save();
    }
}


[Serializable]
public class ProgressData : SavingData
{
    public Action<int> OnHSChanged;
    public Action OnNoAdsBought;

    public int HighScore;
    public bool NoAds;

    public void SetHighscore(int hs)
    {
        HighScore = hs;

        OnHSChanged?.Invoke(hs);

        Save();
    }

    public void BoughtNoAds()
    {
        NoAds = true;

        OnNoAdsBought?.Invoke();

        Save();
    }
}